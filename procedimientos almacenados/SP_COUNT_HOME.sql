DROP PROCEDURE SP_COUNT_HOME;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_COUNT_HOME`(
    OUT outCode varchar(3)
)
BEGIN

	DECLARE countSupervisor INT;
    DECLARE countProvider INT;
    DECLARE countFaculty INT;
    DECLARE countUnit INT;
    DECLARE countAnexo INT;
	DECLARE countStorageCall INT;
    DECLARE countNumberOfCallAnswered INT;
    DECLARE countSpokenSecond INT;
    
    # Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;
    
	SELECT count(idUser) INTO countSupervisor FROM users WHERE idRole = 2;
    SELECT count(idProvider) INTO countProvider FROM providers;
    SELECT count(idFaculty) INTO countFaculty FROM faculties;
    SELECT count(idUnit) INTO countUnit FROM units;
    SELECT count(idAnexo) INTO countAnexo FROM anexos;
    SELECT count(idStorage) INTO countStorageCall FROM storageCall; 
	SELECT count(CASE WHEN sc.src = a.idAnexo AND sc.billsec > 0 THEN sc.idStorage ELSE NULL END ) INTO countNumberOfCallAnswered
	FROM anexos a 
    LEFT JOIN storageCall sc ON a.idAnexo = sc.idAnexo;
	SELECT (SUM(CASE WHEN sc.src = a.idAnexo AND sc.billsec > 0 THEN sc.billsec ELSE 0 END)) / 60 INTO countSpokenSecond
	FROM anexos a 
	LEFT JOIN storageCall sc ON a.idAnexo = sc.idAnexo;
    

	SELECT countSupervisor, countProvider, countFaculty, countUnit, countAnexo, countStorageCall, countNumberOfCallAnswered, countSpokenSecond;
END$$
DELIMITER ;

CALL SP_COUNT_HOME(@salida);
SELECT @salida AS result;