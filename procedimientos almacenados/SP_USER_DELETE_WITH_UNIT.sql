DROP PROCEDURE SP_USER_DELETE_WITH_UNIT;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_USER_DELETE_WITH_UNIT`(
    IN p_idUser int,
    IN p_idNewUser int,
    OUT outCode varchar(3)
)
BEGIN
    
    # Declaración de Cursor
    DECLARE v_idUnit INT;
    DECLARE done BOOLEAN DEFAULT FALSE;
    
    DECLARE cur_unit CURSOR FOR SELECT idUnit FROM units WHERE idUser = p_idUser AND isDeleted = 0;
    
    # Controlamos cualquier excepción
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;


	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	IF p_idUser != p_idNewUser AND 
		EXISTS(SELECT 1 FROM users WHERE idUser = p_idUser AND isDeleted = 0) AND 
		EXISTS(SELECT 1 FROM users WHERE idUser = p_idNewUser AND isDeleted = 0) AND
        EXISTS(SELECT 1 FROM units WHERE idUser = p_idUser AND isDeleted = 0) THEN
    
		UPDATE users SET isDeleted = 1 WHERE idUser = p_idUser;
	
        OPEN cur_unit;
        
        read_loop: LOOP
			FETCH cur_unit INTO v_idUnit;
            
            IF done THEN
				LEAVE read_loop;
            END IF;
            
            UPDATE units SET idUser = p_idNewUser WHERE idUnit = v_idUnit;
        END LOOP;
        
        CLOSE cur_unit;
        
        SET outCode = '000';
        COMMIT;
	ELSE

		SET outCode = '001';
	END IF;
END$$
DELIMITER ;