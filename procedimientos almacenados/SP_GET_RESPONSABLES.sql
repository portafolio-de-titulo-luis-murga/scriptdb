DROP PROCEDURE SP_GET_RESPONSABLES;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_GET_RESPONSABLES`(
    OUT outCode varchar(3)
)
BEGIN
	# Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;
	
    SELECT us.idUser, concat(ud.firstName, " ", ud.lastName) AS nameResponsable
	FROM users us
	JOIN userDetails ud ON us.idUser = ud.idUserDetail
	WHERE us.idRole = 2;
    
END$$
DELIMITER ;

