DROP PROCEDURE SP_CREATE_UNIT;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_CREATE_UNIT`(
    IN p_unitName varchar(200),
    IN p_idFaculty int,
    IN p_idUser int,
    OUT outCode varchar(3)
)
BEGIN
    DECLARE countUnit INT;
    
	SELECT count(idUnit) INTO countUnit FROM units WHERE unitName = TRIM(p_unitName) AND isDeleted = 0;
	IF countUnit > 0 THEN
		# Unidad existe, por lo tanto no es valido para crearse
		SET outCode = '001';
	ELSE
		# Unidad no existe,  así que es posible crearlo
		INSERT INTO units (unitName, idFaculty, idUser) VALUES (TRIM(p_unitName), p_idFaculty, p_idUser );
		SET outCode = '000';
		COMMIT;
	END IF;
END$$
DELIMITER ;


select * from units;
