DROP PROCEDURE SP_USER_DELETE;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_USER_DELETE`(
	IN p_idUser INT,
	OUT outCode varchar(3)
)
BEGIN
	# Contexto: Se utiliza para eliminar un usuario sin relación con unidades

	# Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;

	IF EXISTS(SELECT 1 FROM users WHERE idUser = p_idUser AND isDeleted = 0) THEN
		DELETE FROM userSessions WHERE idUser = p_idUser;
		DELETE FROM userDetails WHERE idUserDetail = p_idUser;
		DELETE FROM users WHERE idUser = p_idUser;
        COMMIT;
		SET outCode = '000';
    ELSE
		SET outCode = '001';
    END IF;
END$$
DELIMITER ;