DROP PROCEDURE IF EXISTS SP_LISTAR_COST_SERVICE_PROVIDER;
DELIMITER //
CREATE PROCEDURE SP_LISTAR_COST_SERVICE_PROVIDER(
	IN p_action char,
    IN p_idService int,
    IN p_idProvider int,
    OUT outCode varchar(3)
)
BEGIN
	DECLARE countProvider INT;
    DECLARE countService INT;
    DECLARE countCost INT;
    
    #Caso de insert
    IF p_action = 'L' THEN
		SELECT count(idProvider) INTO countProvider FROM providers WHERE idProvider = p_idProvider;
        SELECT count(idService) INTO countService FROM services WHERE idService = p_idService;
        #SELECT count(idCostServiceProvider) INTO countCost FROM costServicesProvider WHERE idProvider = p_idProvider AND idService = p_idService;
        
		IF countProvider > 0 AND countService > 0 THEN
			#Me lista solo el servicio indicado
			SELECT s.nameService, c.costService, p.nameProvider, c.idService, c.idProvider
			FROM costServicesProvider c
			JOIN services s ON c.idService = s.idService
			JOIN providers p ON c.idProvider = p.idProvider
            WHERE p.idProvider = p_idProvider AND c.idService = p_idService;
			SET outCode = '000';
            COMMIT;
        ELSE
			#Me lista todos los servicios
			SELECT s.nameService, c.costService, p.nameProvider 
			FROM costServicesProvider c
			JOIN services s ON c.idService = s.idService
			JOIN providers p ON c.idProvider = p.idProvider;
            SET outCode = '001';
        END IF;
	END IF;
END; 
//

CALL SP_LISTAR_COST_SERVICE_PROVIDER('L', 13, 11, @salidaCodigo);