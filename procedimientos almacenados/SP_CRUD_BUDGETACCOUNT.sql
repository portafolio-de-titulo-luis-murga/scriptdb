DROP PROCEDURE IF EXISTS SP_CRUD_BUDGETACCOUNT;
DELIMITER //
CREATE PROCEDURE SP_CRUD_BUDGETACCOUNT(
    IN p_action char,
    IN p_idBudgetAccount INT,
	IN p_budgetAccountName VARCHAR(150),
    IN p_amount INT,
    IN p_idFaculty INT,
    OUT outCode VARCHAR(3)
)
BEGIN

	DECLARE countFaculty INT;
    DECLARE counBudget INT;
    
    # Caso de insert
    IF p_action = 'I' THEN
        SELECT COUNT(idFaculty) INTO countFaculty FROM faculties WHERE idFaculty = p_idFaculty;

        IF countFaculty > 0 THEN
            # Facultad existe
            SELECT COUNT(idBudgetAccount) INTO counBudget FROM budgetAccounts WHERE budgetAccountName = TRIM(p_budgetAccountName);

            IF counBudget > 0 THEN
                # Cuenta presupuestaria existe, por lo tanto no es valido para crearse
                SET outCode = '001';
            ELSE
                # Cuenta presupuestaria no existe, así que es posible crearlo
                INSERT INTO budgetAccounts(budgetAccountName, amount, idFaculty) VALUES(TRIM(p_budgetAccountName), p_amount, p_idFaculty);
                SET outCode = '000';
                COMMIT;
            END IF;
        ELSE
            # Facultad no existe
            SET outCode = '002';
        END IF;

    END IF;

END; 
//