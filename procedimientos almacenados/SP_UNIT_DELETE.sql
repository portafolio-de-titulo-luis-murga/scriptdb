DROP PROCEDURE SP_UNIT_DELETE;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_UNIT_DELETE`(
	IN p_idUnit INT,
	OUT outCode varchar(3)
)
BEGIN
	# Contexto: Se utiliza para eliminar un unidad

	# Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;

	IF EXISTS(SELECT 1 FROM units WHERE idUnit = p_idUnit AND isDeleted = 0) THEN
		UPDATE units SET isDeleted = 1 WHERE idUnit = p_idUnit;
        COMMIT;
		SET outCode = '000';
    ELSE
		SET outCode = '001';
    END IF;
END$$
DELIMITER ;
