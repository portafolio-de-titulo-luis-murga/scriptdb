DROP PROCEDURE SP_PDF_RTRU;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_PDF_RTRU`(
	IN p_idUser INT,
    IN p_fechaInicio varchar(20), 
    IN p_fechaFin varchar(20),
    OUT outCode varchar(3)
)
BEGIN
   
	# Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;

		SELECT 	us.idUser, us.userName, count(distinct u.idUnit) as cantUnidades,
				count(distinct a.idAnexo) as cantAnexos, s.idService, s.nameService,
				COUNT(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService AND sc.billsec > 0 THEN sc.idStorage ELSE NULL END) AS numberOfAnsweredCalls,
				SUM(CASE WHEN sc.src = a.idAnexo THEN sc.billsec ELSE 0 END) AS spokenSeconds,
				((SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) * csp.costService) +
				(COUNT(DISTINCT(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService AND sc.billsec > 0 THEN a.idAnexo ELSE NULL END)) * p.fixedCost)) AS cost
		FROM anexos a
		JOIN units u ON a.idUnit = u.idUnit
		JOIN users us ON u.idUser = us.idUser
		JOIN faculties f ON u.idFaculty = f.idFaculty
		JOIN providers p ON f.idProvider = p.idProvider
		JOIN costServicesProvider csp ON p.idProvider = csp.idProvider
		JOIN services s ON csp.idService = s.idService
		LEFT JOIN storageCall sc ON a.idAnexo = sc.idAnexo AND s.idService = sc.idService AND sc.dateCall BETWEEN p_fechaInicio AND p_fechaFin
        WHERE p.isDeleted = 0 AND f.isDeleted = 0 AND u.isDeleted = 0 AND a.isDeleted = 0 AND us.idUser = p_idUser
		GROUP BY us.idUser, s.idService
        order by u.unitName ASC, a.idAnexo ASC;
END$$
DELIMITER ;
