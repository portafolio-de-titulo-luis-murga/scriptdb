DROP PROCEDURE IF EXISTS SP_GET_ANEXOS;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_GET_ANEXOS`(
)
BEGIN
	-- Si no se proporciona un ID, listar todos los anexos
	SELECT a.idAnexo, f.idCost, f.cost
	FROM fixedCosts f
	JOIN anexos a ON f.idCost = a.idCost;
END$$
DELIMITER ;