DROP PROCEDURE SP_UPDATE_PROVIDER;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_UPDATE_PROVIDER`(
    IN p_idProvider INT,
    IN p_nameProvider VARCHAR(150),
    IN p_id_service1 INT,
    IN p_cost_service1 INT,
    IN p_id_service2 INT,
    IN p_cost_service2 INT,
    IN p_id_service3 INT,
    IN p_cost_service3 INT,
    IN p_fixedCost int,
    OUT outCode VARCHAR(3)
)
BEGIN
    DECLARE countProvider INT;
    DECLARE countNameProvider INT;
    DECLARE countService INT;
    
    # Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;
    
    # Validamos que proveedor exista
	SELECT COUNT(idProvider) INTO countProvider FROM providers WHERE idProvider = p_idProvider;
	IF countProvider > 0 THEN
		SELECT COUNT(idProvider) INTO countNameProvider FROM providers WHERE nameProvider = TRIM(p_nameProvider) AND idProvider <> p_idProvider;
        IF countNameProvider = 0  THEN
			# Provedor existe, se puede actualizar
			SELECT COUNT(idService) INTO countService FROM services WHERE idService = p_id_service1 OR idService = p_id_service2 OR idService = p_id_service3;
			IF countService = 3 THEN
				# Servicios existen
				UPDATE providers SET nameProvider = p_nameProvider, fixedCost = p_fixedCost WHERE idProvider = p_idProvider;
				UPDATE costServicesProvider SET costService = p_cost_service1 WHERE idService = p_id_service1 AND idProvider = p_idProvider;
				UPDATE costServicesProvider SET costService = p_cost_service2 WHERE idService = p_id_service2 AND idProvider = p_idProvider;
				UPDATE costServicesProvider SET costService = p_cost_service3 WHERE idService = p_id_service3 AND idProvider = p_idProvider;
				SET outCode = '000';
				COMMIT;
			ELSE
				SET outCode = '002';
			END IF;
        ELSE
			SET outCode = '004';
        END IF;
		
	ELSE
		# Provedor no existe
		SET outCode = '001';
	END IF;
END$$
DELIMITER ;
