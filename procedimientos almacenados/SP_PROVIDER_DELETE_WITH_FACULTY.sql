DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_PROVIDER_DELETE_WITH_FACULTY`(
    IN p_idProvider int,
    IN p_idNewProvider int,
    OUT outCode varchar(3)
)
BEGIN
    # Requerimiento: No se puede eliminar (cambiar estado) un proveedor que esté asociado a facultades sin antes cambiar el proveedor de esas facultades
    # Este SP se utilizará siempre y cuando el proveedor tenga relación con alguna facultad, en caso contrario, se ejecutará un DELETE al proveedor por medio
    # de otro SP.
    
    # Declaración de Cursor
    DECLARE v_idfaculty INT;
    DECLARE done BOOLEAN DEFAULT FALSE;
    
    DECLARE cur_faculty CURSOR FOR SELECT idFaculty FROM faculties WHERE idProvider = p_idProvider AND isDeleted = 0;
    
    
    # Controlamos cualquier excepción
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;

	# Declarar un manejador para la condición 'NOT FOUND', que se activa cuando el cursor llega al final
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	IF p_idProvider != p_idNewProvider AND 
		EXISTS(SELECT 1 FROM providers WHERE idProvider = p_idProvider AND isDeleted = 0) AND 
		EXISTS(SELECT 1 FROM providers WHERE idProvider = p_idNewProvider AND isDeleted = 0) AND
        EXISTS(SELECT 1 FROM faculties WHERE idProvider = p_idProvider AND isDeleted = 0) THEN
    
		UPDATE providers SET isDeleted = 1 WHERE idProvider = p_idProvider;
		
        # Abrir el cursor
        OPEN cur_faculty;
        
        read_loop: LOOP
			FETCH cur_faculty INTO v_idfaculty;
            
            IF done THEN
				LEAVE read_loop;
            END IF;
            
            UPDATE faculties SET idProvider = p_idNewProvider WHERE idFaculty = v_idfaculty;
        END LOOP;
        
        CLOSE cur_faculty;
        
        SET outCode = '000';
        COMMIT;
	ELSE
		# Algún ID de proveedor no existe y por lo tanto no se puede eliminar
		SET outCode = '001';
	END IF;
END$$
DELIMITER ;
