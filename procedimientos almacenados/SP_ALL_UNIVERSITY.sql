DROP PROCEDURE SP_ALL_UNIVERSITY;
#SP_ALL_UNIVERSITY = Reporte general de manera mensual por toda la universidad.
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_ALL_UNIVERSITY`(
    IN p_fechaInicio varchar(20), 
    IN p_fechaFin varchar(20),
    OUT outCode varchar(3)
)
BEGIN
	# Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;
		SELECT f.idFaculty, f.facultyName, 
		count(CASE WHEN sc.src = a.idAnexo AND sc.billsec > 0 THEN sc.idStorage ELSE NULL END ) AS 'numberOfCall',
		SUM(CASE WHEN sc.src = a.idAnexo AND sc.billsec > 0 THEN sc.billsec ELSE 0 END) AS 'spokenSecond'
		FROM faculties f
		JOIN units u ON f.idFaculty = u.idFaculty
		JOIN anexos a ON u.idUnit = a.idUnit
		LEFT JOIN storageCall sc ON a.idAnexo = sc.idAnexo AND sc.dateCall BETWEEN p_fechaInicio AND p_fechaFin
        WHERE f.isDeleted = 0 AND u.isDeleted = 0 AND a.isDeleted = 0
		GROUP BY f.idFaculty;
END$$
DELIMITER ;