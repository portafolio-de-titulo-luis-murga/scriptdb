DROP PROCEDURE SP_EXCEL_RTRU;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_EXCEL_RTRU`(
	IN p_idUser INT,
    IN p_fechaInicio varchar(20), 
    IN p_fechaFin varchar(20),
    OUT outCode varchar(3)
)
BEGIN
	# Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;
    
		select us.userName, u.unitName, a.idAnexo, s.nameService,
			SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) AS spokenSeconds,
			((SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) * csp.costService) + 
			(CASE WHEN SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) > 0 THEN p.fixedCost ELSE 0 END)) AS 'cost',
			COUNT(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService AND sc.billsec > 0 THEN sc.idStorage ELSE NULL END) AS numberOfAnsweredCalls
		from anexos a
		join units u on a.idUnit = u.idUnit
		join users us on u.idUser = us.idUser
		join faculties f on u.idFaculty = f.idFaculty
		join providers p on f.idProvider = p.idProvider
		join costServicesProvider csp on p.idProvider = csp.idProvider
		join services s on csp.idService = s.idService
		left join storageCall sc on a.idAnexo = sc.idAnexo AND s.idService = sc.idService AND sc.dateCall BETWEEN p_fechaInicio AND p_fechaFin
		WHERE p.isDeleted = 0 AND f.isDeleted = 0 AND u.isDeleted = 0 AND a.isDeleted = 0 AND us.idUser = p_idUser
		GROUP BY us.userName, a.idAnexo, s.nameService
        order by u.unitName ASC, a.idAnexo ASC; 
END$$
DELIMITER ;

