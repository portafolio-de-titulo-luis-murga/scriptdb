DROP PROCEDURE SP_CREATE_PROVIDER;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_CREATE_PROVIDER`(
    IN p_idProvider int,
    IN p_nameProvider varchar(150),
    IN p_fixedCost int,
    OUT outCode varchar(3),
    OUT out_idProvider int
)
BEGIN
    DECLARE countProvider INT;
    
	SELECT count(idProvider) INTO countProvider FROM providers WHERE nameProvider = TRIM(p_nameProvider);
	IF countProvider > 0 THEN
		# Provedor existe, por lo tanto no es valido para crearse
		SET outCode = '001';
	ELSE
		# Provedor no existe,  así que es posible crearlo
		INSERT INTO providers (nameProvider, fixedCost) VALUES (TRIM(p_nameProvider), p_fixedCost);
		SET outCode = '000';
		SET out_idProvider = LAST_INSERT_ID();
		COMMIT;
	END IF;
END$$
DELIMITER ;
