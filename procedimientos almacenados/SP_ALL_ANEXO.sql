DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_ALL_ANEXO`(
	IN p_idAnexo INT,
    IN p_fechaInicio varchar(20), 
    IN p_fechaFin varchar(20),
    OUT outCode varchar(3)
)
BEGIN
   
	# Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;

    
    IF p_idAnexo > 0 THEN
            
        SELECT a.idAnexo, u.unitName, f.facultyName, p.nameProvider, s.idService, s.nameService,
		SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) AS spokenSeconds,
		((SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) * csp.costService) + 
		(CASE WHEN SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) > 0 THEN a.cost ELSE 0 END)) AS 'cost',
		COUNT(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService AND sc.billsec > 0 THEN sc.idStorage ELSE NULL END) AS numberOfAnsweredCalls
		FROM anexos a
		JOIN units u ON a.idUnit = u.idUnit
		JOIN faculties f ON u.idFaculty = f.idFaculty
		JOIN providers p ON f.idProvider = p.idProvider
		JOIN costServicesProvider csp ON p.idProvider = csp.idProvider
		JOIN services s ON csp.idService = s.idService
		LEFT JOIN storageCall sc ON a.idAnexo = sc.idAnexo AND s.idService = sc.idService AND sc.dateCall BETWEEN p_fechaInicio AND p_fechaFin
		WHERE p.isDeleted = 0 AND f.isDeleted = 0 AND u.isDeleted = 0 AND a.isDeleted = 0 AND a.idAnexo = p_idAnexo
		GROUP BY a.idAnexo, s.idService;
        
    ELSE
        
        SELECT a.idAnexo, u.unitName, f.facultyName, p.nameProvider, s.idService, s.nameService,
		SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) AS spokenSeconds,
		((SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) * csp.costService) + 
		(CASE WHEN SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) > 0 THEN a.cost ELSE 0 END)) AS 'cost',
		COUNT(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService AND sc.billsec > 0 THEN sc.idStorage ELSE NULL END) AS numberOfAnsweredCalls
		FROM anexos a
		JOIN units u ON a.idUnit = u.idUnit
		JOIN faculties f ON u.idFaculty = f.idFaculty
		JOIN providers p ON f.idProvider = p.idProvider
		JOIN costServicesProvider csp ON p.idProvider = csp.idProvider
		JOIN services s ON csp.idService = s.idService
		LEFT JOIN storageCall sc ON a.idAnexo = sc.idAnexo AND s.idService = sc.idService AND sc.dateCall BETWEEN p_fechaInicio AND p_fechaFin
		WHERE p.isDeleted = 0 AND f.isDeleted = 0 AND u.isDeleted = 0 AND a.isDeleted = 0
		GROUP BY a.idAnexo, s.idService;
        
    END IF;

END$$
DELIMITER ;
