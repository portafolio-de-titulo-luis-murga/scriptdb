DROP PROCEDURE IF EXISTS SP_CRUD_USER;
DELIMITER //
CREATE PROCEDURE SP_CRUD_USER(
    IN p_action char,
    IN p_idUser int,
    IN p_idRole int,
    IN p_username varchar(15),
    IN p_pwd varchar(200),
    IN p_firstName varchar(50),
    IN p_lastName varchar(50),
    OUT outCode VARCHAR(3)
)
BEGIN
    DECLARE countRole INT;
    DECLARE countUser INT;
    DECLARE lastIdUser INT;

    # Caso de insert
    IF p_action = 'I' THEN
        SELECT COUNT(idRole) INTO countRole FROM roles WHERE idRole = p_idRole;

        IF countRole > 0 THEN
            # Rol existe
            SELECT COUNT(idUser) INTO countUser FROM users WHERE userName = TRIM(p_username);

            IF countUser > 0 THEN
                # Usuario existe, por lo tanto no es valido para crearse
                SET outCode = '001';
            ELSE
                # Usuario no existe, así que es posible crearlo
                INSERT INTO users(userName, pwdUser, idRole) VALUES(TRIM(p_username), p_pwd, p_idRole);
                SET lastIdUser = LAST_INSERT_ID();
                INSERT INTO userDetails(idUserDetail, firstName, lastName) VALUES(lastIdUser, p_firstName, p_lastName);
                SET outCode = '000';
                COMMIT;
            END IF;
        ELSE
            # Rol no existe
            SET outCode = '002';
        END IF;

    END IF;

END; 
//