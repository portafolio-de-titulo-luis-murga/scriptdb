DROP PROCEDURE SP_GET_ROLE;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_GET_ROLE`(
	IN p_idRole INT
)
BEGIN
	IF p_idRole IS NULL OR p_idRole = 0 THEN
		SELECT idRole, roleName, description FROM roles
        ORDER BY roleName ASC;
	ELSE
		SELECT idRole, roleName, description FROM roles
		WHERE idRole = p_idRole
        ORDER BY roleName ASC;
	END IF;
END$$
DELIMITER ;