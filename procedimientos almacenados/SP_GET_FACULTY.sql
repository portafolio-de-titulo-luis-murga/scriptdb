DROP PROCEDURE SP_GET_FACULTY;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_GET_FACULTY`(
	IN p_idFaculty INT
)
BEGIN
	IF p_idFaculty IS NULL OR p_idFaculty = 0 THEN
	-- Si no se proporciona un ID, listar todos
		SELECT idFaculty, facultyName FROM faculties
		WHERE isDeleted = 0;
    ELSE
		SELECT idFaculty, facultyName FROM faculties
		WHERE idFaculty = p_idFaculty AND isDeleted = 0;
    END IF;
END$$
DELIMITER ;