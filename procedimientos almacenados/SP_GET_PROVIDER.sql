DROP PROCEDURE SP_GET_PROVIDER;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_GET_PROVIDER`(
    IN p_idProvider INT
)
BEGIN
    IF p_idProvider IS NULL OR p_idProvider = 0 THEN
        -- Si no se proporciona un ID, listar todos los proveedores
        SELECT p.idProvider, p.nameProvider, p.fixedCost, (CASE WHEN f.idFaculty IS NULL THEN 0 ELSE 1 END) AS hasRelationFaculties, 
        s.idService, s.nameService, c.costService
		FROM costServicesProvider c
		JOIN services s ON c.idService = s.idService
		JOIN providers p ON c.idProvider = p.idProvider
		LEFT JOIN faculties f ON p.idProvider = f.idProvider
		WHERE p.isDeleted = 0
		GROUP BY p.idProvider, s.idService
		ORDER BY p.idProvider ASC, s.idService ASC;

    ELSE
        -- Si se proporciona un ID, listar solo ese proveedor
        SELECT p.idProvider, p.nameProvider, p.fixedCost, (CASE WHEN f.idFaculty IS NULL THEN 0 ELSE 1 END) AS hasRelationFaculties, 
        s.idService, s.nameService, c.costService
        FROM costServicesProvider c
        JOIN services s ON c.idService = s.idService
        JOIN providers p ON c.idProvider = p.idProvider
        LEFT JOIN faculties f ON p.idProvider = f.idProvider
        WHERE p.idProvider = p_idProvider AND p.isDeleted = 0
        GROUP BY p.idProvider, s.idService
        ORDER BY s.idService ASC;
    END IF;
END$$
DELIMITER ;
