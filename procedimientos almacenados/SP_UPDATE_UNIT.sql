DROP PROCEDURE SP_UPDATE_UNIT;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_UPDATE_UNIT`(
    IN p_idUnit int,
    IN p_unitName varchar(200),
    IN p_idFaculty int,
	IN p_idUser int,
    OUT outCode varchar(3)
)
BEGIN
	DECLARE countUnit INT;
    DECLARE countUnitName INT;
    
    # Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;
    
    # Validamos que unidad exista    
	SELECT count(idUnit) INTO countUnit FROM units WHERE idUnit = p_idUnit AND isDeleted = 0;
	IF countUnit > 0 THEN
		SELECT count(idUnit) INTO countUnitName FROM units WHERE idUnit != p_idUnit AND unitName = TRIM(p_unitName) AND isDeleted = 0;
        IF countUnitName > 0 THEN
			SET outCode = '002';
		ELSE   
			UPDATE units 
			SET unitName = p_unitName, idFaculty = p_idFaculty, idUser = p_idUser 
			WHERE idUnit = p_idUnit;
			SET outCode = '000';
			COMMIT;
        END IF;
	ELSE
		SET outCode = '001';
	END IF;

END$$
DELIMITER ;