DROP PROCEDURE SP_GET_UNIT;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_GET_UNIT`(
	IN p_idUnit INT
)
BEGIN
	IF p_idUnit IS NULL OR p_idUnit = 0 THEN
    -- Si no se proporciona un ID, listar todos los anexos de cada unidad
        select u.idUnit, u.unitName, f.idFaculty, f.facultyName, us.idUser ,concat(ud.firstName, " ", ud.lastName) AS nameUser, a.idAnexo
        from anexos a
        join units u on a.idUnit = u.idUnit
        join faculties f on u.idFaculty = f.idFaculty
        join users us on u.idUser = us.idUser
        join userDetails ud on us.idUser = ud.idUserDetail
		WHERE u.isDeleted = 0
		group by u.idUnit, a.idAnexo
        order by u.idUnit;
	ELSE
        -- Si se proporciona un ID, listar solo ese anexo con su unidad
        select u.idUnit, u.unitName, f.idFaculty, f.facultyName, us.idUser ,concat(ud.firstName, " ", ud.lastName) AS nameUser, count(a.idAnexo) as numberOfAnexo
        from anexos a
        join units u on a.idUnit = u.idUnit
        join faculties f on u.idFaculty = f.idFaculty
        join users us on u.idUser = us.idUser
        join userDetails ud on us.idUser = ud.idUserDetail
		where u.idUnit = p_idUnit AND u.isDeleted = 0
        group by u.idUnit;
    END IF;
END$$
DELIMITER ;
