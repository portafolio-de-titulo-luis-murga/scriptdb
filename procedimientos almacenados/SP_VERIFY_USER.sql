DROP PROCEDURE SP_VERIFY_USER;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_VERIFY_USER`(
    IN nameUser VARCHAR(15), 
    OUT outCode VARCHAR(3)
)
BEGIN
    DECLARE count INT;

    SELECT COUNT(idUser) INTO count FROM users WHERE isDeleted = 0 AND userName = nameUser;

    IF count > 0 THEN
        SET outCode = '000';
    ELSE
        SET outCode = '001';
    END IF;

	SELECT u.idUser, u.userName, u.pwdUser, u.email, r.idRole, r.roleName
	FROM users u 
	JOIN roles r 
	ON (u.idRole = r.idRole)
	WHERE u.isDeleted = 0 AND u.userName = nameUser;

END$$
DELIMITER ;