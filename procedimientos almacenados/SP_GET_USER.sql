DROP PROCEDURE SP_GET_USER;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_GET_USER`(
    IN p_idUser INT
)
BEGIN
    IF p_idUser IS NULL OR p_idUser = 0 THEN
        -- Si no se proporciona un ID, listar todos los usuarios
		SELECT us.idUser, us.userName, d.firstName, d.lastName, us.email, r.idRole, r.roleName, (CASE WHEN u.idUser IS NULL THEN 0 ELSE 1 END) AS hasRelationUnits
		FROM users us
		LEFT JOIN units u ON us.idUser = u.idUser
		JOIN roles r ON us.idRole = r.idRole
		JOIN userDetails d ON us.idUser = d.idUserDetail
		WHERE us.isDeleted = 0
		GROUP BY us.idUser
		ORDER BY r.roleName ASC, us.idUser ASC;

    ELSE
        -- Si se proporciona un ID, listar solo ese usuarios
        SELECT us.idUser, us.userName, d.firstName, d.lastName, us.email, r.idRole, r.roleName, (CASE WHEN u.idUser IS NULL THEN 0 ELSE 1 END) AS hasRelationUnits
		FROM users us
		LEFT JOIN units u ON us.idUser = u.idUser
		JOIN roles r ON us.idRole = r.idRole
		JOIN userDetails d ON us.idUser = d.idUserDetail
		WHERE us.idUser = p_idUser AND us.isDeleted = 0
		GROUP BY us.idUser
		ORDER BY r.roleName ASC, us.idUser ASC;
    END IF;
END$$
DELIMITER ;
