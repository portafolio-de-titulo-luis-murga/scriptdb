DROP PROCEDURE IF EXISTS SP_CRUD_COST_SERVICE_PROVIDER;
DELIMITER //
CREATE PROCEDURE SP_CRUD_COST_SERVICE_PROVIDER(
	IN p_action char,
    IN p_idService int,
    IN p_costService int,
    IN p_idProvider int,
    OUT outCode varchar(3)
)
BEGIN
	DECLARE countProvider INT;
    DECLARE countService INT;
    DECLARE countCost INT;
    
    #Caso de insert
    IF p_action = 'I' THEN
		SELECT count(idProvider) INTO countProvider FROM providers WHERE idProvider = p_idProvider;
        SELECT count(idService) INTO countService FROM services WHERE idService = p_idService;
        SELECT count(idCostServiceProvider) INTO countCost FROM costServicesProvider WHERE idProvider = p_idProvider AND idService = p_idService;
        
		IF countProvider > 0 AND countService > 0 AND countCost = 0 THEN
			# Provedor es exactamente el mismo ingresado en el primer SP, por lo tanto es valido para ingresar costos a sus servicios
            INSERT INTO costServicesProvider (costService, idProvider, idService) VALUES (p_costService, p_idProvider, p_idService);
			SET outCode = '000';
            COMMIT;
        ELSE
			# Provedor no es el mismo al ingresar el primer SP,  así que no es posible ingresar los costos a los servicio
            SET outCode = '001';
        END IF;
	END IF;
END; 
//

CALL SP_CRUD_COST_SERVICE_PROVIDER('I', 13, 700, 11, @salidaCodigo);