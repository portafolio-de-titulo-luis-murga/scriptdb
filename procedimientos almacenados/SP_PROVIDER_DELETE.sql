DROP PROCEDURE IF EXISTS SP_PROVIDER_DELETE;
DELIMITER //
CREATE PROCEDURE SP_PROVIDER_DELETE(
    IN p_idProvider INT,
    OUT outCode varchar(3)
)
BEGIN
    # Contexto: Se utiliza para eliminar un proveedor que no esté asociado a facultades

    # Controlamos cualquier excepción
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        SET outCode = '900';
    END;

    IF EXISTS(SELECT 1 FROM providers WHERE idProvider = p_idProvider) THEN
        DELETE FROM costServicesProvider WHERE idProvider = p_idProvider;
        DELETE FROM providers WHERE idProvider = p_idProvider;
        SET outCode = '000';
        COMMIT;
    ELSE
        SET outCode = '001';
    END IF;
END; 
//
DELIMITER ;