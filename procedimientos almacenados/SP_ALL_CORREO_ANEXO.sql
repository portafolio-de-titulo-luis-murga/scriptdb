DROP PROCEDURE IF EXISTS SP_ALL_CORREO_ANEXO;
DELIMITER //
CREATE PROCEDURE SP_ALL_CORREO_ANEXO(
    IN p_fechaInicio varchar(20), 
    IN p_fechaFin varchar(20),
    OUT outCode varchar(3)
)
BEGIN
		# Controlamos cualquier excepción
		DECLARE EXIT HANDLER FOR SQLEXCEPTION
		BEGIN
			SET outCode = '900';
		END;

		SELECT count(DISTINCT(a.idAnexo)) AS 'countAnexos', 
		COUNT(CASE WHEN IFNULL(CAST(sc.src AS UNSIGNED), 0) = a.idAnexo THEN sc.idStorage ELSE NULL END) AS 'numberOfCalls',
		SUM(CASE WHEN IFNULL(CAST(sc.src AS UNSIGNED), 0) = a.idAnexo THEN sc.billsec ELSE 0 END) AS 'secondOfCalls',
		SUM(CASE WHEN csp.idService = 12 AND IFNULL(CAST(sc.src AS UNSIGNED), 0) = a.idAnexo THEN sc.billsec  ELSE 0 END) AS 'spokenCEL',
		(SUM(CASE WHEN csp.idService = 12 AND IFNULL(CAST(sc.src AS UNSIGNED), 0) = a.idAnexo THEN sc.billsec ELSE 0 END) * csp.costService) AS 'costCEL',
		COUNT(CASE WHEN csp.idService = 12 AND IFNULL(CAST(sc.src AS UNSIGNED), 0) = a.idAnexo THEN sc.idStorage ELSE NULL END) AS 'numberOfCallsCEL',
		SUM(CASE WHEN csp.idService = 13 AND IFNULL(CAST(sc.src AS UNSIGNED), 0) = a.idAnexo THEN sc.billsec  ELSE 0 END) AS 'spokenSLM',
		(SUM(CASE WHEN csp.idService = 13 AND IFNULL(CAST(sc.src AS UNSIGNED), 0) = a.idAnexo THEN sc.billsec ELSE 0 END) * csp.costService) AS 'costSLM',
		COUNT(CASE WHEN csp.idService = 13 AND IFNULL(CAST(sc.src AS UNSIGNED), 0) = a.idAnexo THEN sc.idStorage ELSE NULL END) AS 'numberOfCallsSLM',
		SUM(CASE WHEN csp.idService = 14 AND IFNULL(CAST(sc.src AS UNSIGNED), 0) = a.idAnexo THEN sc.billsec  ELSE 0 END) AS 'spokenLDI',
		(SUM(CASE WHEN csp.idService = 14 AND IFNULL(CAST(sc.src AS UNSIGNED), 0) = a.idAnexo THEN sc.billsec ELSE 0 END) * csp.costService) AS 'costLDI',
		COUNT(CASE WHEN csp.idService = 14 AND IFNULL(CAST(sc.src AS UNSIGNED), 0) = a.idAnexo THEN sc.idStorage ELSE NULL END) AS 'numberOfCallsLDI'
		FROM anexos a
		JOIN serviceAnexo sa ON a.idAnexo = sa.idAnexo
		LEFT JOIN storageCall sc ON sa.idServiceAnexo = sc.idServiceAnexo
		JOIN costServicesProvider csp ON sa.idCostServiceProvider = csp.idCostServiceProvider
        WHERE sc.dateCall BETWEEN p_fechaInicio AND p_fechaFin;

END; 
//
DELIMITER ;

CALL SP_ALL_CORREO_ANEXO('2023-01-08', '2023-01-15', @sal);