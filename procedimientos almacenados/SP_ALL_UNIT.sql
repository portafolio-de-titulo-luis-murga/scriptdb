DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_ALL_UNIT`(
	IN p_idUnit INT,
    IN p_fechaInicio varchar(20), 
    IN p_fechaFin varchar(20),
    OUT outCode varchar(3)
)
BEGIN
   
	# Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;

    
    IF p_idUnit > 0 THEN
            
        SELECT u.idUnit, u.unitName, us.userName, f.facultyName, p.nameProvider, COUNT(DISTINCT(a.idAnexo)) AS countAnexos, 
		p.fixedCost AS fixedCostAnexos, s.idService, s.nameService, csp.costService,
		SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) AS spokenSeconds,
		((SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) * csp.costService) +
		(COUNT(DISTINCT(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService AND sc.billsec > 0 THEN a.idAnexo ELSE NULL END)) * p.fixedCost)) AS cost,
		COUNT(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService AND sc.billsec > 0 THEN sc.idStorage ELSE NULL END) AS numberOfAnsweredCalls
		FROM units u
		JOIN users us ON u.idUser = us.idUser
		JOIN faculties f ON u.idFaculty = f.idFaculty
		JOIN providers p ON f.idProvider = p.idProvider
		JOIN costServicesProvider csp ON p.idProvider = csp.idProvider
		JOIN services s ON csp.idService = s.idService
		RIGHT JOIN anexos a ON u.idUnit = a.idUnit
		LEFT JOIN storageCall sc ON a.idAnexo = sc.idAnexo AND s.idService = sc.idService AND sc.dateCall BETWEEN p_fechaInicio AND p_fechaFin
        WHERE p.isDeleted = 0 AND f.isDeleted = 0 AND u.isDeleted = 0 AND a.isDeleted = 0 AND u.idUnit = p_idUnit
		GROUP BY u.idUnit, s.idService;
        
    ELSE
        
        SELECT u.idUnit, u.unitName, us.userName, f.facultyName, p.nameProvider, COUNT(DISTINCT(a.idAnexo)) AS countAnexos, 
		p.fixedCost AS fixedCostAnexos, s.idService, s.nameService, csp.costService,
		SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) AS spokenSeconds,
		((SUM(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService THEN sc.billsec ELSE 0 END) * csp.costService) +
		(COUNT(DISTINCT(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService AND sc.billsec > 0 THEN a.idAnexo ELSE NULL END)) * p.fixedCost)) AS cost,
		COUNT(CASE WHEN sc.src = a.idAnexo AND sc.idService = s.idService AND sc.billsec > 0 THEN sc.idStorage ELSE NULL END) AS numberOfAnsweredCalls
		FROM units u
		JOIN users us ON u.idUser = us.idUser
		JOIN faculties f ON u.idFaculty = f.idFaculty
		JOIN providers p ON f.idProvider = p.idProvider
		JOIN costServicesProvider csp ON p.idProvider = csp.idProvider
		JOIN services s ON csp.idService = s.idService
		RIGHT JOIN anexos a ON u.idUnit = a.idUnit
		LEFT JOIN storageCall sc ON a.idAnexo = sc.idAnexo AND s.idService = sc.idService AND sc.dateCall BETWEEN p_fechaInicio AND p_fechaFin
        WHERE p.isDeleted = 0 AND f.isDeleted = 0 AND u.isDeleted = 0 AND a.isDeleted = 0
		GROUP BY u.idUnit, s.idService;
        
    END IF;

END$$
DELIMITER ;
