DROP PROCEDURE IF EXISTS SP_INSERT_STORAGECALL;
DELIMITER //
CREATE PROCEDURE SP_INSERT_STORAGECALL(
	OUT outCode varchar(3)
)
BEGIN
  # Contexto: Se utiliza para insertar llamadas en tabla storageCall
  
  # Declaración de variables
  DECLARE countServiceAnexo INT;
  DECLARE i INT DEFAULT 0;
  DECLARE bucleCount INT;
  DECLARE idServiceAnexoSelected INT;
  DECLARE idAnexoSelected INT;
  DECLARE anexoVarchar VARCHAR(10);
  DECLARE randomValue INT;
  DECLARE billsecRandom INT DEFAULT 0;
  DECLARE callWaiting INT;
  DECLARE phoneNumber VARCHAR(20);
  DECLARE callStatus VARCHAR(15);
  DECLARE WhoCall VARCHAR(20);

  # Controlamos cualquier excepción
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    SET outCode = '900';
  END;
  
  SELECT COUNT(idServiceAnexo) INTO countServiceAnexo FROM serviceAnexo;
  
  IF countServiceAnexo >= 6 THEN
    SET bucleCount = CEIL(RAND() * countServiceAnexo);
	  WHILE i < bucleCount DO
  
      # Se obtiene una fila aleatoria de la tabla serviceAnexo
      SELECT idServiceAnexo, idAnexo INTO idServiceAnexoSelected, idAnexoSelected FROM serviceAnexo ORDER BY RAND() LIMIT 1;

      # Se convierte id de anexo de entero a texto
      SET anexoVarchar = CONVERT(idAnexoSelected, CHAR(10));
      # Se genera un valor aleatorio entre 1 y 30 como tiempo de espera a contestar una llamada
      SET callWaiting = FLOOR(RAND() * (30 - 1 + 1) + 1);

      # Se evalua si el otro número es anexo o celular (valor 1 dice que fue anexo)
      SET randomValue = CEIL(RAND() * 2);
      IF randomValue = 1 THEN
        SET WhoCall = 'ANEXO';
        SET phoneNumber = (SELECT CONVERT(idAnexo, CHAR(10)) FROM serviceAnexo WHERE idAnexo <> idAnexoSelected ORDER BY RAND() LIMIT 1);
      ELSE 
        # Se genera un número de teléfono al azar
        SET WhoCall = 'CELLPHONE';
        SET phoneNumber = CONCAT('(569)', SUBSTRING(CAST(RAND() * 100000000 AS CHAR), 1, 8));
      END IF;

      # Se evalua si la llamada fue contestada o no
      SET randomValue = CEIL(RAND() * 2);
      IF randomValue = 1 THEN
        # Valor aleatorio entre 20 y 600
        SET billsecRandom = FLOOR(RAND() * (600 - 20 + 1) + 20);
        SET callWaiting = callWaiting + billsecRandom;
        SET callStatus = 'Contestada';
      ELSE
        SET billsecRandom = 0;
        SET callWaiting = callWaiting + billsecRandom;
        SET callStatus = 'No Contestada';
      END IF;
    
      # Si el otro número es ANEXO, entonces siempre será origen el anexo principal
      IF WhoCall = 'ANEXO' THEN
        INSERT INTO storageCall(src, dst, dateCall, duration, billsec, answer, idServiceAnexo)
        VALUES (anexoVarchar, phoneNumber, CURDATE(), callWaiting, billsecRandom, callStatus, idServiceAnexoSelected);
      ELSE
        # Se evalua si anexo llamó o fue llamado (valor igual a 1 dice que anexo llamó)
        SET randomValue = CEIL(RAND() * 2);
        IF randomValue = 1 THEN
          INSERT INTO storageCall(src, dst, dateCall, duration, billsec, answer, idServiceAnexo)
          VALUES (anexoVarchar, phoneNumber, CURDATE(), callWaiting, billsecRandom, callStatus, idServiceAnexoSelected);
        ELSE
          INSERT INTO storageCall(src, dst, dateCall, duration, billsec, answer, idServiceAnexo)
          VALUES (phoneNumber, anexoVarchar, CURDATE(), callWaiting, billsecRandom, callStatus, idServiceAnexoSelected);
        END IF;
      END IF;

      COMMIT;
      SET i = i + 1;
	  END WHILE;
	END IF;
  SET outCode = '000';
END; 
//
DELIMITER ;