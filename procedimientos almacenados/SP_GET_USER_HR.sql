DROP PROCEDURE SP_GET_USER_HR;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_GET_USER_HR`(
    IN p_idUser INT,
    OUT outCode varchar(3)
)
BEGIN

	# Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;
    
    IF EXISTS(SELECT 1 FROM users WHERE idUser = p_idUser) THEN
    
		SELECT us.idUser, us.userName, us.idRole, (CASE WHEN u.idUser IS NULL THEN 0 ELSE 1 END) AS hasRelationUnits
		from users us
		left join units u on us.idUser = u.idUser
		order by us.idUser asc;
        
        SET outCode = '000';
        
    ELSE
    
		SET outCode = '001';   
        
    END IF;
END$$
DELIMITER ;
