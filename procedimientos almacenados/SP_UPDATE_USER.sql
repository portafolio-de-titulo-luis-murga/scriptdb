DROP PROCEDURE SP_UPDATE_USER;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_UPDATE_USER`(
	IN p_idUser INT,
	IN p_firstName VARCHAR(50),
    IN p_lastName VARCHAR(50),
    IN p_email VARCHAR(250),
    OUT outCode VARCHAR(3)
)
BEGIN
    DECLARE countUser INT;
    
    # Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;
    
    # Validamos que usuario exista
	SELECT COUNT(idUser) INTO countUser FROM users WHERE idUser = p_idUser AND isDeleted = 0;
	IF countUser > 0 THEN
		UPDATE users SET email = p_email WHERE idUser = p_idUser;
		UPDATE userDetails SET firstName = p_firstName, lastName = p_lastName WHERE idUserDetail = p_idUser;
		SET outCode = '000';
		COMMIT;
	ELSE
		SET outCode = '001';
	END IF;

END$$
DELIMITER ;







