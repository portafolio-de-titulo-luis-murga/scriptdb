DROP PROCEDURE SP_RESET_PW;
DELIMITER $$
CREATE DEFINER=`murgaf`@`%` PROCEDURE `SP_RESET_PW`(
	IN p_idUser INT,
    IN p_resetPw varchar(200),
    OUT outCode varchar(3)
)
BEGIN
	DECLARE countUser INT;

	# Controlamos cualquier excepción
	DECLARE EXIT HANDLER FOR SQLEXCEPTION
	BEGIN
		SET outCode = '900';
	END;
    
    SELECT COUNT(idUser) INTO countUser FROM users WHERE idUser = p_idUser AND isDeleted = 0 AND idRole = 2;
    # Validamos si usuario es supervisor y si está activo
    IF countUser > 0 THEN
        UPDATE users SET pwdUser = p_resetPw WHERE idUser = p_idUser;
        commit;
        
        SELECT email from users where idUser = p_idUser;
        
		SET outCode = '000';
	else
		SET outCode = '001';
    END IF;

END$$
DELIMITER ;