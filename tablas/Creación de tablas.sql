create table roles(
    idRole int auto_increment primary key,
    roleName varchar(50) not null,
    description varchar(200) null
);

create table users(
    idUser int auto_increment primary key,
    userName varchar(15) unique not null,
    pwdUser varchar(200) not null,
    email varchar(250) not null,
    isDeleted tinyint default 0,
    idRole int not null
);

create table userSessions(
    idUserSession int auto_increment primary key,
    refreshToken varchar(200) not null,
    jwtId varchar(200) not null,
    idUser int not null
);

create table userDetails(
    idUserDetail int primary key,
    lastName varchar(50),
    firstName varchar(50)
);

create table providers(
    idProvider int auto_increment primary key,
    nameProvider varchar(150) not null,
    isDeleted tinyint default 0
);

create table services(
    idService int auto_increment primary key,
    nameService varchar(150) not null,
    description varchar(100) null
);

create table costServicesProvider(
    idCostServiceProvider int auto_increment primary key,
    costService int not null,
    idProvider int not null,
    idService int not null
);

create table faculties(
    idFaculty int auto_increment primary key,
    facultyName varchar(200) not null,
    isDeleted tinyint default 0,
    idProvider int not null
);

create table units(
    idUnit int auto_increment primary key,
    unitName varchar(200) not null,
    isDeleted tinyint default 0,
    idFaculty int not null,
    idUser int not null
);

create table budgetAccounts(
    idBudgetAccount int auto_increment primary key,
    budgetAccountName varchar(200) not null,
    amount int not null,
    isDeleted tinyint default 0,
    idUnit int null
);

create table anexos(
    idAnexo int primary key,
    cost int not null,
    isDeleted tinyint default 0,
    idUnit int not null
);

create table storageCall(
    idStorage int auto_increment primary key,
    src int not null,
    dst int not null,
    dateCall date,
    duration int not null,
    billsec int not null,
    answer varchar(15) not null,
    idAnexo int not null,
    idService int not null
);