

/* INSERT DE TABLA REGISTRO DE LLAMADAS */
insert into storageCall (src, dst, dateCall, duration, billsec, answer, idAnexo) values (100,"(569)93677889",'2023-02-05',283,222,"Contestada",100);



/* INSERT DE TABLA ANEXOS */
insert into anexos (idAnexo, idUnit, idCost) values (100, 1, 1);
insert into anexos (idAnexo, idUnit, idCost) values (101, 1, 1);
insert into anexos (idAnexo, idUnit, idCost) values (102, 1, 1);
insert into anexos (idAnexo, idUnit, idCost) values (103, 2, 2);
insert into anexos (idAnexo, idUnit, idCost) values (104, 2, 2);
insert into anexos (idAnexo, idUnit, idCost) values (105, 2, 2);
insert into anexos (idAnexo, idUnit, idCost) values (106, 4, 1);
insert into anexos (idAnexo, idUnit, idCost) values (107, 4, 1);
insert into anexos (idAnexo, idUnit, idCost) values (108, 4, 1);
insert into anexos (idAnexo, idUnit, idCost) values (109, 4, 1);
insert into anexos (idAnexo, idUnit, idCost) values (110, 5, 1);
insert into anexos (idAnexo, idUnit, idCost) values (111, 5, 1);
insert into anexos (idAnexo, idUnit, idCost) values (112, 5, 1);
insert into anexos (idAnexo, idUnit, idCost) values (113, 5, 1);
insert into anexos (idAnexo, idUnit, idCost) values (114, 6, 1);
insert into anexos (idAnexo, idUnit, idCost) values (115, 6, 1);
insert into anexos (idAnexo, idUnit, idCost) values (116, 6, 1);
insert into anexos (idAnexo, idUnit, idCost) values (117, 6, 1);
insert into anexos (idAnexo, idUnit, idCost) values (118, 7, 1);
insert into anexos (idAnexo, idUnit, idCost) values (119, 7, 1);
insert into anexos (idAnexo, idUnit, idCost) values (120, 7, 1);
insert into anexos (idAnexo, idUnit, idCost) values (200, 7, 1);
insert into anexos (idAnexo, idUnit, idCost) values (210, 8, 1);
insert into anexos (idAnexo, idUnit, idCost) values (220, 8, 1);
insert into anexos (idAnexo, idUnit, idCost) values (230, 8, 1);
insert into anexos (idAnexo, idUnit, idCost) values (240, 8, 1);
insert into anexos (idAnexo, idUnit, idCost) values (250, 9, 1);
insert into anexos (idAnexo, idUnit, idCost) values (260, 9, 1);
insert into anexos (idAnexo, idUnit, idCost) values (270, 9, 1);
insert into anexos (idAnexo, idUnit, idCost) values (280, 9, 1);
insert into anexos (idAnexo, idUnit, idCost) values (290, 9, 1);
insert into anexos (idAnexo, idUnit, idCost) values (300, 10, 2);

insert into anexos (idAnexo, idUnit, idCost) values (310, 10, 2);
insert into anexos (idAnexo, idUnit, idCost) values (320, 10, 2);
insert into anexos (idAnexo, idUnit, idCost) values (330, 10, 2);
insert into anexos (idAnexo, idUnit, idCost) values (340, 10, 2);
insert into anexos (idAnexo, idUnit, idCost) values (350, 11, 2);
insert into anexos (idAnexo, idUnit, idCost) values (360, 11, 2);
insert into anexos (idAnexo, idUnit, idCost) values (370, 12, 2);
insert into anexos (idAnexo, idUnit, idCost) values (380, 12, 2);
insert into anexos (idAnexo, idUnit, idCost) values (390, 13, 2);
insert into anexos (idAnexo, idUnit, idCost) values (400, 13, 2);
insert into anexos (idAnexo, idUnit, idCost) values (500, 14, 2);
insert into anexos (idAnexo, idUnit, idCost) values (510, 14, 2);
insert into anexos (idAnexo, idUnit, idCost) values (520, 15, 2);
insert into anexos (idAnexo, idUnit, idCost) values (530, 15, 2);
insert into anexos (idAnexo, idUnit, idCost) values (540, 16, 2);
insert into anexos (idAnexo, idUnit, idCost) values (550, 16, 2);
insert into anexos (idAnexo, idUnit, idCost) values (560, 17, 2);
insert into anexos (idAnexo, idUnit, idCost) values (570, 18, 2);
insert into anexos (idAnexo, idUnit, idCost) values (580, 18, 2);
insert into anexos (idAnexo, idUnit, idCost) values (590, 19, 2);
insert into anexos (idAnexo, idUnit, idCost) values (600, 20, 2);
insert into anexos (idAnexo, idUnit, idCost) values (610, 20, 2);
insert into anexos (idAnexo, idUnit, idCost) values (620, 21, 2);
insert into anexos (idAnexo, idUnit, idCost) values (630, 21, 2);



/* INSERT DE TABLA USUARIOS */
insert into users (userName, pwdUser, idRole) values ("lrojas", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("ahermosi", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("cbruna", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("jmoram", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("gigonzal", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("lcelis", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("jeperfor", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("lhinojosa", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("abarbag", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("cmiranda", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("cfajardo", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("xmena", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("asiri", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("egomez", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("degly", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("dmanzano", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("mgaetee", 12345, 2);
insert into users (userName, pwdUser, idRole) values ("nescobar", 12345, 2);


/* INSERT DE TABLA FACULTADES */
insert into faculties (facultyName, idProvider) values ("Facultad de Ciencias Veterinarias", 14);
insert into faculties (facultyName, idProvider) values ("Facultad de Ciencias Sociales", 22);
insert into faculties (facultyName, idProvider) values ("Facultad de Comunicaciones", 25);
insert into faculties (facultyName, idProvider) values ("Facultad de Educación", 14);
insert into faculties (facultyName, idProvider) values ("Facultad de Economía y Administración ", 22);
insert into faculties (facultyName, idProvider) values ("Facultad de Derecho ", 25);
insert into faculties (facultyName, idProvider) values ("Facultad de Filosofía ", 14);
insert into faculties (facultyName, idProvider) values ("Facultad de Física ", 22);
insert into faculties (facultyName, idProvider) values ("Facultad de Historia ", 25);
insert into faculties (facultyName, idProvider) values ("Facultad de Ingeniería ", 14);
insert into faculties (facultyName, idProvider) values ("Facultad de Letras ", 22);
insert into faculties (facultyName, idProvider) values ("Facultad de Matemáticas ", 25);
insert into faculties (facultyName, idProvider) values ("Facultad de Urbanismo", 14);
insert into faculties (facultyName, idProvider) values ("Facultad de Química ", 22);
insert into faculties (facultyName, idProvider) values ("Facultad de Farmacia ", 25);
insert into faculties (facultyName, idProvider) values ("Facultad de Teología ", 14);
insert into faculties (facultyName, idProvider) values ("Facultad de Geografía ", 22);
insert into faculties (facultyName, idProvider) values ("Facultad de Ciencia Política ", 25);



/* INSERT DE TABLA UNIDADES */
insert into units (unitName, idFaculty, idUser) values ("Unidad de Ciencias Veterinarias", 6, 5);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Ciencias Sociales", 8, 6);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Comunicaciones", 5, 7);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Educación", 5, 8);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Economía y Administración", 18, 9);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Derecho", 24, 10);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Filosofía ", 24, 11);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Física", 22, 12);
insert into units (unitName, idFaculty, idUser)values ("Unidad de Historia", 22, 13);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Ingeniería ", 22, 14);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Letras", 22, 15);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Matemáticas ", 22, 16);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Urbanismo", 22, 17);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Química", 22, 18);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Farmacia", 23, 19);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Teología", 23, 20);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Geografía", 24, 21);
insert into units (unitName, idFaculty, idUser) values ("Unidad de Ciencia Política", 24, 22);

INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,100);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,100);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,100);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,101);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,101);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,101);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,102);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,102);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,102);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,103);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,103);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,103);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,104);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,104);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,104);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,105);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,105);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,105);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,106);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,106);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,106);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,107);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,107);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,107);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,108);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,108);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,108);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,109);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,109);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,109);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,110);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,110);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,110);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,111);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,111);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,111);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,112);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,112);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,112);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,113);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,113);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,113);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,114);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,114);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,114);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,115);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,115);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,115);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,116);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,116);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,116);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,117);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,117);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,117);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,118);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,118);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,118);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,119);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,119);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,119);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,120);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,120);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,120);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,200);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,200);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,200);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,210);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,210);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,210);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,220);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,220);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,220);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,230);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,230);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,230);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,240);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,240);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,240);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,250);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,250);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,250);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,260);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,260);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,260);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,270);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,270);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,270);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,280);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,280);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,280);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,290);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,290);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,290);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,300);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,300);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,300);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,310);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,310);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,310);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,320);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,320);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,320);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,330);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,330);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,330);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,340);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,340);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,340);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(12,350);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(13,350);
INSERT INTO serviceAnexo(idService, idAnexo) VALUES(14,350);










