ALTER TABLE userSessions ADD CONSTRAINT fk_userSessions_users
FOREIGN KEY(idUser) REFERENCES users(idUser);

ALTER TABLE users ADD CONSTRAINT fk_users_roles 
FOREIGN key(idRole) REFERENCES roles(idRole);

ALTER TABLE userDetails ADD constraint fk_userDetails_users 
FOREIGN KEY(idUserDetail) REFERENCES users(idUser);

ALTER TABLE anexos ADD CONSTRAINT fk_anexos_units
FOREIGN KEY(idUnit) REFERENCES units(idUnit);

ALTER TABLE units ADD CONSTRAINT fk_units_faculties
FOREIGN KEY(idFaculty) REFERENCES faculties(idFaculty);

ALTER TABLE units ADD CONSTRAINT fk_units_users
FOREIGN KEY(idUser) REFERENCES users(idUser);

ALTER TABLE budgetAccounts ADD CONSTRAINT fk_budgetAccounts_units
FOREIGN KEY(idUnit) REFERENCES units(idUnit);

ALTER TABLE faculties ADD CONSTRAINT fk_faculties_providers
FOREIGN KEY(idProvider) REFERENCES providers(idProvider);

ALTER TABLE costServicesProvider ADD CONSTRAINT fk_costServicesProvider_providers
FOREIGN KEY(idProvider) REFERENCES providers(idProvider);

ALTER TABLE costServicesProvider ADD CONSTRAINT fk_costServicesProvider_services
FOREIGN KEY(idService) REFERENCES services(idService);

ALTER TABLE storageCall ADD CONSTRAINT fk_storageCall_anexos
FOREIGN KEY(idAnexo) REFERENCES anexos(idAnexo);

ALTER TABLE storageCall ADD CONSTRAINT fk_storageCall_services
FOREIGN KEY(idService) REFERENCES services(idService);